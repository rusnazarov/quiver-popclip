Quiver
===
[Quiver](http://happenapps.com/#quiver) extension for PopClip.

* [PopClip](http://pilotmoon.com/popclip/)
* [PopClip Extensions Developer Documentation](http://pilotmoon.com/popclip/extensions/extensions-docs.html)
* [quiver: Specification](https://github.com/HappenApps/Quiver/wiki/Quiver-Data-Format)

## Download
[Quiver.popclipextz](https://bitbucket.org/rusnazarov/quiver-popclip/downloads/Quiver.popclipextz)

## Credit
Extension created by @[rnazarov](https://twitter.com/rnazarov 'Contact me on Twitter')